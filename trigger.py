#!/usr/bin/env python3

import os
import json
import argparse
from rpi_rf import RFDevice

parser = argparse.ArgumentParser(description='Send 433MHz codes from json config')
parser.add_argument('signal_name', type=str, help='Name of signal to send from codes.json')
args = parser.parse_args()

class RFTransmit:
  def __init__(self, tx_pin):
    self.tx_pin = tx_pin

  def __enter__(self):
    self.device = RFDevice(self.tx_pin)
    self.device.enable_tx()
    return self.device

  def __exit__(self, type, value, traceback):
    self.device.cleanup()

class RfConfig:
  def __init__(self, config_directory):
    self.config_directory = config_directory
    self.config = self.load_json('config.json')
    self.codes = self.load_json('codes.json')
    self.tx_pin = self.config['pins']['transmit']

  def load_json(self, file):
    path = os.path.join(self.config_directory, file)
    with open(path, "r") as json_file:
      data = json.load(json_file)
    return data

  def lookup_signal(self, signal_name):
    return self.codes[signal_name]

config = RfConfig(os.path.dirname(__file__))
signal_code = config.lookup_signal(args.signal_name)
print(f'Sending "{args.signal_name}": {signal_code}')

with RFTransmit(config.tx_pin) as radio:
  radio.tx_repeat = 3
  radio.tx_code(signal_code)
